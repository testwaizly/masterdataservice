import { Controller } from '@nestjs/common';
import { UsersService } from './users.service';
import {
  Ctx,
  MessagePattern,
  Payload,
  RmqContext,
} from '@nestjs/microservices';

@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}
  @MessagePattern('masterdata-get-users')
  async getUsers(@Payload() body: any, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMsg = context.getMessage();
    channel.ack(originalMsg);
    const data = await this.usersService.getUsers(body);
    return data;
  }
  @MessagePattern('masterdata-create-users')
  async createUsers(@Payload() body: any, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMsg = context.getMessage();
    channel.ack(originalMsg);
    const data = await this.usersService.createUsers(body);
    return data;
  }
  @MessagePattern('masterdata-update-users')
  async updateUsers(@Payload() body: any, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMsg = context.getMessage();
    channel.ack(originalMsg);
    const data = await this.usersService.updateUsers(body);
    return data;
  }
  @MessagePattern('masterdata-delete-users')
  async deleteUsers(@Payload() body: any, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMsg = context.getMessage();
    channel.ack(originalMsg);
    const data = await this.usersService.deleteUsers(body);
    return data;
  }
  @MessagePattern('masterdata-get-single-users')
  async getSingleUsers(@Payload() body: any, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMsg = context.getMessage();
    channel.ack(originalMsg);
    const data = await this.usersService.getSingleUsers(body);
    return data;
  }
  @MessagePattern('masterdata-get-single-users-by-username')
  async getSingleUsersByUsername(
    @Payload() body: any,
    @Ctx() context: RmqContext,
  ) {
    const channel = context.getChannelRef();
    const originalMsg = context.getMessage();
    channel.ack(originalMsg);
    const data = await this.usersService.getSingleUsersByUsername(body);
    return data;
  }
}
