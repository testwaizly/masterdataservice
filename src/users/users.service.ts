import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UsersEntity } from '../models/users.entity';
import { RpcException } from '@nestjs/microservices';
import * as Sentry from '@sentry/node';
@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UsersEntity)
    private readonly usersRepository: Repository<UsersEntity>,
  ) {}
  async getUsers(query) {
    try {
      if (query.code != 'ROL_COMPANY') {
        const findOptions: any = {
          take: query.total,
          skip: query.from,
          order: {
            insertedAt: 'DESC',
          },
        };
        const data = await this.usersRepository.find(findOptions);
        const total = await this.usersRepository.count();
        return { data: data, total: total };
      }
    } catch (error) {
      Sentry.captureException(new Error(error));
      throw new RpcException({
        statusCode: 500,
        message: error.message,
      });
    }
  }
  async createUsers(body) {
    try {
      const date = new Date();
      // // Converts the UTC time to a locale specific format, including adjusting for timezone. oke

      const currentDateTimeCentralTimeZone = date.toLocaleString('sv', {
        timeZone: 'Asia/Jakarta',
      });
      const result = {
        ...body,
        insertedAt: currentDateTimeCentralTimeZone,
      };
      const data = await this.usersRepository.insert(result);
      return data;
    } catch (error) {
      Sentry.captureException(new Error(error));
      throw new RpcException({
        statusCode: 500,
        message: error.message,
      });
    }
  }
  async updateUsers(body) {
    try {
      const date = new Date();
      // // Converts the UTC time to a locale specific format, including adjusting for timezone. oke

      const currentDateTimeCentralTimeZone = date.toLocaleString('sv', {
        timeZone: 'Asia/Jakarta',
      });
      const result = {
        ...body,
        updatedAt: currentDateTimeCentralTimeZone,
      };
      delete result.id;
      const data = await this.usersRepository.update(
        { id_users: body.id },
        result,
      );
      return data;
    } catch (error) {
      Sentry.captureException(new Error(error));
      throw new RpcException({
        statusCode: 500,
        message: error.message,
      });
    }
  }
  async deleteUsers(id) {
    try {
      return await this.usersRepository.delete({ id_users: id });
    } catch (error) {
      Sentry.captureException(new Error(error));
      throw new RpcException({
        statusCode: 500,
        message: error.message,
      });
    }
  }
  async getSingleUsers(id) {
    try {
      return this.usersRepository.findOne({
        where: {
          id_users: id,
        },
      });
    } catch (error) {
      Sentry.captureException(new Error(error));
      throw new RpcException({
        statusCode: 500,
        message: error.message,
      });
    }
  }
  async getSingleUsersByUsername(body) {
    try {
      return this.usersRepository.findOne({
        where: {
          email: body.email,
          is_active: body.is_active,
        },
      });
    } catch (error) {
      Sentry.captureException(new Error(error));
      throw new RpcException({
        statusCode: 500,
        message: error.message,
      });
    }
  }
}
