import { Module } from '@nestjs/common';
import { EmployeeController } from './employee.controller';
import { EmployeeService } from './employee.service';
import { EmployeeEntity } from 'src/models/employee.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([EmployeeEntity], 'default')],
  controllers: [EmployeeController],
  providers: [EmployeeService],
})
export class EmployeeModule {}
