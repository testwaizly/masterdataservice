import { Injectable } from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';
import { InjectRepository } from '@nestjs/typeorm';
import { EmployeeEntity } from 'src/models/employee.entity';
import { Repository } from 'typeorm';
import * as Sentry from '@sentry/node';
@Injectable()
export class EmployeeService {
  constructor(
    @InjectRepository(EmployeeEntity)
    private readonly employeeRepository: Repository<EmployeeEntity>,
  ) {}
  async getEmployee(query) {
    try {
      const findOptions: any = {
        take: query.total,
        skip: query.from,
        order: {
          insertedAt: 'DESC',
        },
      };
      const data = await this.employeeRepository.find(findOptions);
      const total = await this.employeeRepository.count();
      return { data: data, total: total };
    } catch (error) {
      Sentry.captureException(new Error(error));
      throw new RpcException({
        statusCode: 500,
        message: error.message,
      });
    }
  }
  async createEmployee(body) {
    try {
      const date = new Date();
      // // Converts the UTC time to a locale specific format, including adjusting for timezone. oke
      const currentDateTimeCentralTimeZone = date.toLocaleString('sv', {
        timeZone: 'Asia/Jakarta',
      });
      console.log(body);
      const result = {
        ...body,
        insertedAt: currentDateTimeCentralTimeZone,
      };
      const data = await this.employeeRepository.insert(result);
      return data;
    } catch (error) {
      Sentry.captureException(new Error(error));
      throw new RpcException({
        statusCode: 500,
        message: error.message,
      });
    }
  }
  async updateEmployee(body) {
    try {
      const date = new Date();
      // // Converts the UTC time to a locale specific format, including adjusting for timezone. oke
      const currentDateTimeCentralTimeZone = date.toLocaleString('sv', {
        timeZone: 'Asia/Jakarta',
      });
      const result = {
        ...body,
        updatedAt: currentDateTimeCentralTimeZone,
      };
      delete result.id;
      const data = await this.employeeRepository.update(
        { id_employee: body.id },
        result,
      );
      return data;
    } catch (error) {
      Sentry.captureException(new Error(error));
      throw new RpcException({
        statusCode: 500,
        message: error.message,
      });
    }
  }
  async deleteEmployee(id) {
    try {
      return await this.employeeRepository.delete({ id_employee: id });
    } catch (error) {
      Sentry.captureException(new Error(error));
      throw new RpcException({
        statusCode: 500,
        message: error.message,
      });
    }
  }
  async getSingleEmployee(id) {
    try {
      return this.employeeRepository.findOne({
        where: {
          id_employee: id,
        },
      });
    } catch (error) {
      Sentry.captureException(new Error(error));
      throw new RpcException({
        statusCode: 500,
        message: error.message,
      });
    }
  }
}
