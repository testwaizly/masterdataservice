import { Controller } from '@nestjs/common';
import {
  Ctx,
  MessagePattern,
  Payload,
  RmqContext,
} from '@nestjs/microservices';
import { EmployeeService } from './employee.service';

@Controller('employee')
export class EmployeeController {
  constructor(private employeeService: EmployeeService) {}
  @MessagePattern('masterdata-get-employee')
  async getCompanies(@Payload() body: any, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMsg = context.getMessage();
    channel.ack(originalMsg);
    const data = await this.employeeService.getEmployee(body);
    return data;
  }
  @MessagePattern('masterdata-create-employee')
  async createCompanies(@Payload() body: any, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMsg = context.getMessage();
    channel.ack(originalMsg);
    const data = await this.employeeService.createEmployee(body);
    return data;
  }
  @MessagePattern('masterdata-update-employee')
  async updateCompanies(@Payload() body: any, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMsg = context.getMessage();
    channel.ack(originalMsg);
    const data = await this.employeeService.updateEmployee(body);
    return data;
  }
  @MessagePattern('masterdata-delete-employee')
  async deleteCompanies(@Payload() body: any, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMsg = context.getMessage();
    channel.ack(originalMsg);
    const data = await this.employeeService.deleteEmployee(body);
    return data;
  }
  @MessagePattern('masterdata-get-single-employee')
  async getSingleCompanies(@Payload() body: any, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMsg = context.getMessage();
    channel.ack(originalMsg);
    const data = await this.employeeService.getSingleEmployee(body);
    return data;
  }
}
