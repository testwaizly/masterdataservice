import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { RolesEntity } from '../models/roles.entity';
import { Repository } from 'typeorm';
import { RpcException } from '@nestjs/microservices';
import * as Sentry from '@sentry/node';
@Injectable()
export class RolesService {
  constructor(
    @InjectRepository(RolesEntity)
    private readonly rolesRepository: Repository<RolesEntity>,
  ) {}
  async getRoles(query) {
    try {
      const findOptions: any = {
        take: query.total,
        skip: query.from,
        // where: {
        //     id_car:LessThan(query.from)
        // },
        order: {
          insertedAt: 'DESC',
        },
      };
      const data = await this.rolesRepository.find(findOptions);
      const total = await this.rolesRepository.count();
      return { data: data, total: total };
    } catch (error) {
      Sentry.captureException(new Error(error));
      throw new RpcException({
        statusCode: 500,
        message: error.message,
      });
    }
  }
  async createRoles(body) {
    try {
      const date = new Date();
      // // Converts the UTC time to a locale specific format, including adjusting for timezone. oke

      const currentDateTimeCentralTimeZone = date.toLocaleString('sv', {
        timeZone: 'Asia/Jakarta',
      });
      const result = {
        ...body,
        insertedAt: currentDateTimeCentralTimeZone,
      };
      const data = await this.rolesRepository.insert(result);
      return data;
    } catch (error) {
      Sentry.captureException(new Error(error));
      throw new RpcException({
        statusCode: 500,
        message: error.message,
      });
    }
  }
  async updateRoles(body) {
    try {
      const date = new Date();
      // // Converts the UTC time to a locale specific format, including adjusting for timezone. oke

      const currentDateTimeCentralTimeZone = date.toLocaleString('sv', {
        timeZone: 'Asia/Jakarta',
      });
      const result = {
        ...body,
        updatedAt: currentDateTimeCentralTimeZone,
      };
      delete result.id;
      const data = await this.rolesRepository.update(
        { id_roles: body.id },
        result,
      );
      return data;
    } catch (error) {
      Sentry.captureException(new Error(error));
      throw new RpcException({
        statusCode: 500,
        message: error.message,
      });
    }
  }
  async deleteRoles(id) {
    try {
      return await this.rolesRepository.delete({ id_roles: id });
    } catch (error) {
      Sentry.captureException(new Error(error));
      throw new RpcException({
        statusCode: 500,
        message: error.message,
      });
    }
  }
  async getSingleRoles(id) {
    try {
      return this.rolesRepository.findOne({
        where: {
          id_roles: id,
        },
      });
    } catch (error) {
      Sentry.captureException(new Error(error));
      throw new RpcException({
        statusCode: 500,
        message: error.message,
      });
    }
  }
  async searchRoles(body) {
    try {
      return this.rolesRepository.findOne({
        where: {
          code: body.code,
        },
      });
    } catch (error) {
      Sentry.captureException(new Error(error));
      throw new RpcException({
        statusCode: 500,
        message: error.message,
      });
    }
  }
}
