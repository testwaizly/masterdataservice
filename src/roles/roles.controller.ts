import { Controller } from '@nestjs/common';
import { RolesService } from './roles.service';
import {
  Ctx,
  MessagePattern,
  Payload,
  RmqContext,
} from '@nestjs/microservices';

@Controller('roles')
export class RolesController {
  constructor(private rolesService: RolesService) {}
  @MessagePattern('masterdata-get-roles')
  async getRoles(@Payload() body: any, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMsg = context.getMessage();
    channel.ack(originalMsg);
    const data = await this.rolesService.getRoles(body);
    return data;
  }
  @MessagePattern('masterdata-create-roles')
  async createRoles(@Payload() body: any, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMsg = context.getMessage();
    channel.ack(originalMsg);
    const data = await this.rolesService.createRoles(body);
    return data;
  }
  @MessagePattern('masterdata-update-roles')
  async updateRoles(@Payload() body: any, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMsg = context.getMessage();
    channel.ack(originalMsg);
    const data = await this.rolesService.updateRoles(body);
    return data;
  }
  @MessagePattern('masterdata-delete-roles')
  async deleteRoles(@Payload() body: any, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMsg = context.getMessage();
    channel.ack(originalMsg);
    const data = await this.rolesService.deleteRoles(body);
    return data;
  }
  @MessagePattern('masterdata-get-single-roles')
  async getSingleROles(@Payload() body: any, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMsg = context.getMessage();
    channel.ack(originalMsg);
    const data = await this.rolesService.getSingleRoles(body);
    return data;
  }
  @MessagePattern('masterdata-search-roles')
  async searchRoles(@Payload() body: any, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMsg = context.getMessage();
    channel.ack(originalMsg);
    const data = await this.rolesService.searchRoles(body);
    return data;
  }
}
