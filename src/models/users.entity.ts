import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { RolesEntity } from './roles.entity';
@Entity({ name: 'tb_users' })
export class UsersEntity {
  @PrimaryGeneratedColumn({ name: 'id_users' })
  id_users: number;

  @Column({
    name: 'first_name',
    type: 'varchar',
    length: 100,
    nullable: false,
  })
  first_name: string;

  @Column({
    name: 'last_name',
    type: 'varchar',
    length: 100,
    nullable: false,
  })
  last_name: string;

  @Column({ name: 'is_active', type: 'char', length: 1, nullable: false })
  is_active: string;

  @Column({ name: 'is_login', type: 'char', length: 1, nullable: false })
  is_login: string;

  @Column({
    name: 'email',
    type: 'varchar',
    length: 100,
    nullable: false,
    unique: true,
  })
  email: string;

  @Column({ name: 'id_roles', type: 'bigint', nullable: false })
  id_roles: string;
  @ManyToOne(() => RolesEntity, (asset) => asset.users, {
    cascade: true,
    eager: true,
  })
  @JoinColumn({ name: 'id_roles' })
  roles: RolesEntity;
  @Column({
    name: 'password',
    type: 'varchar',
    length: 100,
    nullable: false,
  })
  password: string;

  @CreateDateColumn({ name: 'inserted_at' })
  insertedAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;
}
