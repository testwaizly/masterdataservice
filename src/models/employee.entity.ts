import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
@Entity({ name: 'tb_employee' })
export class EmployeeEntity {
  @PrimaryGeneratedColumn({ name: 'id_employee' })
  id_employee: number;

  @Column({
    name: 'name',
    type: 'varchar',
    length: 100,
    nullable: false,
  })
  name: string;

  @Column({
    name: 'job_title',
    type: 'varchar',
    length: 100,
    nullable: false,
  })
  job_title: string;

  @Column({
    name: 'salary',
    type: 'decimal',
    precision: 10,
    scale: 2,
    nullable: false,
  })
  salary: number;

  @Column({
    name: 'departement',
    type: 'varchar',
    length: 100,
    nullable: false,
  })
  departement: string;

  @Column({ name: 'joined_date', nullable: true })
  joined_date: Date;

  @CreateDateColumn({ name: 'inserted_at' })
  insertedAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;
}
