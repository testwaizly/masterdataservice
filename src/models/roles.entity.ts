import { UsersEntity } from 'src/models/users.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
@Entity({ name: 'tb_roles' })
export class RolesEntity {
  @PrimaryGeneratedColumn({ name: 'id_roles' })
  id_roles: number;

  @Column({
    name: 'name',
    type: 'varchar',
    length: 100,
    nullable: false,
  })
  name: string;

  @Column({
    name: 'code',
    type: 'varchar',
    length: 100,
    nullable: false,
  })
  code: string;

  @CreateDateColumn({ name: 'inserted_at' })
  insertedAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  @OneToMany(() => UsersEntity, (data) => data.roles)
  users: UsersEntity;
}
