import envsub from 'envsub';
import { join } from 'path';

const YAML_CONFIG_FILENAME = './config.yaml';
const {
  PORT,
  //Rabbitmq
  RABBITMQ_USER,
  RABBITMQ_PASSWORD,
  RABBITMQ_HOST,
  RABBITMQ_QUEUE_NAME,
  //database
  DATABASE_HOST,
  DATABASE_PORT,
  DATABASE_USERNAME,
  DATABASE_PASSWORD,
  DATABASE_NAME,
  DATABASE_TYPE,

  //service
  SERVICE_QUERY,
  SERVICE_MASTERDATA,
  //Imagekit
  IMAGEKIT_PUBLICKEY,
  IMAGEKIT_PRIVATEKEY,
  IMAGEKIT_ENDPOINT,

  SENTRY_DSN,
} = process.env;

const templateFile = join(__dirname, YAML_CONFIG_FILENAME);
const outputFile = join(__dirname, YAML_CONFIG_FILENAME);

const options = {
  all: false,
  diff: false,
  envs: [
    // port
    { name: 'PORT', value: PORT },
    //rabbitmq config
    { name: 'RABBITMQ_USER', value: RABBITMQ_USER },
    { name: 'RABBITMQ_PASSWORD', value: RABBITMQ_PASSWORD },
    { name: 'RABBITMQ_HOST', value: RABBITMQ_HOST },
    { name: 'RABBITMQ_QUEUE_NAME', value: RABBITMQ_QUEUE_NAME },
    // mysql
    { name: 'DATABASE_HOST', value: DATABASE_HOST },
    { name: 'DATABASE_PORT', value: DATABASE_PORT },
    { name: 'DATABASE_USERNAME', value: DATABASE_USERNAME },
    { name: 'DATABASE_PASSWORD', value: DATABASE_PASSWORD },
    { name: 'DATABASE_NAME', value: DATABASE_NAME },
    { name: 'DATABASE_TYPE', value: DATABASE_TYPE },
    //service
    { name: 'SERVICE_QUERY', value: SERVICE_QUERY },
    { name: 'SERVICE_MASTERDATA', value: SERVICE_MASTERDATA },

    //imagekit
    { name: 'IMAGEKIT_PUBLICKEY', value: IMAGEKIT_PUBLICKEY },
    { name: 'IMAGEKIT_PRIVATEKEY', value: IMAGEKIT_PRIVATEKEY },
    { name: 'IMAGEKIT_ENDPOINT', value: IMAGEKIT_ENDPOINT },

    { name: 'SENTRY_DSN', value: SENTRY_DSN },
  ],
  envFiles: [join(__dirname, YAML_CONFIG_FILENAME)],
  protect: false,
  syntax: 'default',
  system: true,
};

// create (or overwrite) the output file
export const envObjStart = () =>
  envsub({ templateFile, outputFile, options })
    .then(() => {
      console.log('env-sub has loaded');
    })
    .catch((err: Error) => {
      console.error(err.message);
    });
